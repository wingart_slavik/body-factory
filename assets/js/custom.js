$(function(){
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')



//---- INCLUDE ----
var owlBrandJs 			= $(".owl_brand_js"),
	owlProdJs 			= $(".owl_prod_js"),
	mainSlider			= $('.main_slider_js'),
	thumbSlider			= $(".product_slider"),
	owlVert				= $(".owl_vert"),
	matchHeight 		= $('[data-mh]'),
	starRating 			= $(".star_rating"),
	stripeRating 		= $(".stripe_rating"),
	styler 				= $(".styler"),
	accordion   		= $(".accordion"),
	tabs   				= $(".horizontalTab"),
	mask				= $('.mask'),
	swiperBrandDouble	= $('.swiper_brand_double_js'),
	scrollpane			= $('.scroll-pane'),
	popup 				= $("[data-popup]"),
	
	

	owl 			= $(".owl-carousel"),
	searchSection 	= $(".search"),
	header 			= $('header[role="banner"]'),
	skroll			= $('.skroll'),
	tooltip 	 	= $(".tooltip");

if (owlBrandJs.length || thumbSlider.length || owlProdJs.length || owlVert.length || mainSlider.length){
	include("assets/plugins/owl.carousel.js");
	include("../assets/plugins/owl.carousel.js");
}
if (matchHeight.length){
	include("assets/plugins/jquery.matchHeight.js");
	include("../assets/plugins/jquery.matchHeight.js");
}
if (starRating.length || stripeRating.length){
  include("assets/plugins/jquery.barrating.min.js");
  include("../assets/plugins/jquery.barrating.min.js");
}
if (styler.length){
	include("assets/plugins/formstyler.min.js");
	include("../assets/plugins/formstyler.min.js");
}

if (tabs.length){
	include("assets/plugins/jquery.responsiveTabs.min.js");
	include("../assets/plugins/jquery.responsiveTabs.min.js");
}

if(mask.length){
	include("assets/plugins/inputmask.js");
	include("../assets/plugins/inputmask.js");
}

if(swiperBrandDouble.length){
	include("assets/plugins/swiper.js");
	include("../assets/plugins/swiper.js");
}

if(scrollpane.length){
	include("../assets/plugins/jquery.jscrollpane.min.js");
	include("assets/plugins/jquery.jscrollpane.min.js");
	include("assets/plugins/mwheelIntent.js");
	include("../assets/plugins/mwheelIntent.js");
}

if (popup.length){
	include("assets/plugins/jquery.arcticmodal.js");
	include("../assets/plugins/jquery.arcticmodal.js");
}

// if (owl.length || mainSlider.length){
//   include("assets/plugins/owl.carousel.js");
// }
// if(tooltip.length){
// 	include("assets/plugins/tooltipster.bundle.min.js");
// }

// if (popupBtn.length){
// 	include("assets/plugins/jquery.arcticmodal-0.3.min.js");
// }

// if(skroll.length){
// 	include("assets/plugins/jquery.jscrollpane.js");
// 	include("assets/plugins/jquery.mousewheel.js");
// }






// if (select.length){
// 	include("../assets/plugins/select2.full.js");
// }


function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}


//---- INICIALIZATION ----

$(document).ready(function(){

	$(".menu_btn, .menu_close").on('click ontouchstart', function(e){
  		var body = $('body');
  		
  		body.toggleClass('nav_overlay');
    })
    $(".menu2_btn, .secondary_nav__list__close").on('click ontouchstart', function(e){
  		var body = $('body');
  		
  		body.toggleClass('nav2_overlay');
    })

    $(".menu3_btn, .menu3_btn_close").on('click ontouchstart', function(e){
  		var body = $('body');
  		
  		body.toggleClass('nav3_overlay');
    })

    $(".menu4_btn, .menu4_btn_close").on('click ontouchstart', function(e){
  		var body = $('body');
  		
  		body.toggleClass('nav5_overlay');
    })


    $(".nutrition_menu, .mob_close").on('click ontouchstart', function(e){
  		var body = $('body');
  		
  		body.toggleClass('nav4_overlay');
    })


	$(".filter_btn").on('mouseenter ontouchstart', function(e){
  		var body = $('body'),
  			filterDropdown = $('.filter_dropdown');
  		
  		body.addClass('filter_overlay');

  		$(filterDropdown).on('mouseleave', function(ev){
  			body.removeClass('filter_overlay');
  		})
  		
  		e.preventDefault();
    })

    var menuTimeOutId;

    var menuHandler = {
    	init: function() {

    		var _self = this;

    		this.updateDocState();

    		this.reinit();

    		$(window).on('resize.MenuResize', function(){

    			if(_self.resizeTimeoutId) clearTimeout( _self.resizeTimeoutId );

    			_self.resizeTimeoutId = setTimeout( function(){

    				_self.updateDocState();
    				_self.reinit();

    			}, 50 );

    		});

    	},
    	updateDocState: function(){
    		this.wWidth = $(window).width();
    	},
    	reinit: function() {
    		if( this.wWidth <= 1023 ) {
    			this.initMobileState();
    		}
    		else {
    			this.initDesktopState();
    		}
    	},
    	initMobileState: function(){

    		$('.main_menu').off('.MenuDesktop').on('click.MenuMobile', 'li', function(e){
				var $this = $(this),
					$ul = $this.children('.main_menu--sub_menu');

					e.stopPropagation();			

					if($ul.length) {
						$this.addClass('active');
						e.preventDefault();
					}

				}).on('click', '.first_item_close', function(e){

						var $this = $(this);
						$this.closest("li.active").removeClass('active');

						e.preventDefault();
						e.stopPropagation();

			});

    	},
    	initDesktopState: function(){
    		$(".main_menu").off('.MenuMobile').on('mouseenter.MenuDesktop', 'li' , function(e){
				var $this = $(this),
					$ul = $this.children('.main_menu--sub_menu');

					if($ul.length) {
						$this.addClass('active');
					}

			}).on('mouseleave.MenuDesktop', 'li', function(e){

					var $this = $(this),
						$ul = $this.children('.main_menu--sub_menu'),
						$relTarget = $(e.relatedTarget);


					if($ul.length && $relTarget.closest($ul).length) return;

					
					$this.removeClass('active');

			});
    	}
    };

    menuHandler.init();


	

	// if(screen.width <= 1023){

	// 	$('.main_menu').on('click', 'li', function(e){
	// 		var $this = $(this),
	// 			$ul = $this.children('.main_menu--sub_menu');

	// 			e.stopPropagation();			

	// 			if($ul.length) {
	// 				$this.addClass('active');
	// 				e.preventDefault();
	// 			}

	// 		}).on('click', '.first_item_close', function(e){

	// 				var $this = $(this);
	// 				$this.closest("li.active").removeClass('active');

	// 				e.preventDefault();
	// 				e.stopPropagation();

	// 	});

	// }
	// else {
	// 	$(".main_menu").on('mouseenter', 'li' , function(e){
	// 		var $this = $(this),
	// 			$ul = $this.children('.main_menu--sub_menu');

	// 			if($ul.length) {
	// 				$this.addClass('active');
	// 			}

	// 	}).on('mouseleave', 'li', function(e){

	// 			var $this = $(this),
	// 				$ul = $this.children('.main_menu--sub_menu'),
	// 				$relTarget = $(e.relatedTarget);


	// 			if($ul.length && $relTarget.closest($ul).length) return;

				
	// 			$this.removeClass('active');

	// 	});
	// }


    // $(", .first_item_close").on('mouseover ontouchstart', function(e){
  		// var body = $('body');

  		// $(this).parent().prev('.blk');


  		// $(this).toggleClass("active");

  		// $(this).nextSibling("ul").find(".first_item_close");

  		
  		// body.toggleClass('nav4_overlay');
    // })


	/*------------   Scrollpane   ------------*/

		if(scrollpane.length){
			scrollpane.jScrollPane({
				getIsScrollableV: true
			});
		}

	/*------------   Scrollpane END   ------------*/


	/*------------   OWL Brand   ------------*/

		if(owlBrandJs.length){
			owlBrandJs.owlCarousel({
		        items: 5,
		        navigation: true,
		      //   responsive:{
				    //     0:{
				    //         items:1
				    //     },
				    //     500:{
				    //         items:1
				    //     },
				    //     1028:{
				    //         items:2
				    //     }
				    // }

				itemsDesktop : [1024,4], 
				itemsDesktopSmall : [995,3], 
				itemsTablet: [768,2], 
				itemsMobile : [480,1] ,
			});
		}

	/*------------   OWL Brand END   ------------*/


	/*------------   OWL Prod   ------------*/

		if(owlProdJs.length){
			owlProdJs.owlCarousel({
		        items: 3,
		        navigation: true,
		        responsive:{
				        0:{
				            items:1
				        },
				        600:{
				            items:2
				        },
				        1025:{
				            items:3
				        }
				    }
			});
		}

	/*------------   OWL Prod END   ------------*/


	/*------------   OWL Brand   ------------*/

		if(mainSlider.length){
			mainSlider.owlCarousel({
				mouseDrag: false,
				items : 1, 
				itemsDesktop : [1000,1], 
				itemsDesktopSmall : [900,1], 
				itemsTablet: [600,1], 
				itemsMobile : [480,1] ,
				autoPlay : 5000
			});
		}

	/*------------   OWL Brand END   ------------*/

	
	/*------------   OWL thumbs product   ------------*/

		if(thumbSlider.length){
			  var sync1 = $("#sync1");
			  var sync2 = $("#sync2");
			 
			  sync1.owlCarousel({
			    singleItem : true,
			    slideSpeed : 1000,
			    navigation: true,
			    pagination:false,
			    afterAction : syncPosition,
			    responsiveRefreshRate : 200,
			  });
			 
			  sync2.owlCarousel({
			    // items : 3,
			    // itemsDesktop      : [1199,10],
			    // itemsDesktopSmall     : [979,10],
			    // itemsTablet       : [768,8],
			    // itemsMobile       : [479,4],
			    pagination:false,
			    responsiveRefreshRate : 100,
			    afterInit : function(el){
			      el.find(".owl-item").eq(0).addClass("synced");
			    }
			  });
			 
			  function syncPosition(el){
			    var current = this.currentItem;
			    $("#sync2")
			      .find(".owl-item")
			      .removeClass("synced")
			      .eq(current)
			      .addClass("synced")
			    if($("#sync2").data("owlCarousel") !== undefined){
			      center(current)
			    }
			  }
			 
			  $("#sync2").on("click", ".owl-item", function(e){
			    e.preventDefault();
			    var number = $(this).data("owlItem");
			    sync1.trigger("owl.goTo",number);
			  });
			 
			  function center(number){
			    var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
			    var num = number;
			    var found = false;
			    for(var i in sync2visible){
			      if(num === sync2visible[i]){
			        var found = true;
			      }
			    }
			 
			    if(found===false){
			      if(num>sync2visible[sync2visible.length-1]){
			        sync2.trigger("owl.goTo", num - sync2visible.length+2)
			      }else{
			        if(num - 1 === -1){
			          num = 0;
			        }
			        sync2.trigger("owl.goTo", num);
			      }
			    } else if(num === sync2visible[sync2visible.length-1]){
			      sync2.trigger("owl.goTo", sync2visible[1])
			    } else if(num === sync2visible[0]){
			      sync2.trigger("owl.goTo", num-1)
			    }
			    
			  }
		}

	/*------------   OWL thumbs product END   ------------*/


	/*------------   OWL thumbs product   ------------*/

		if(owlVert.length){
			  var sync1 = $("#sync1");
			  var sync2 = $("#sync2");
			 
			  sync1.owlCarousel({
			    singleItem : true,
			    slideSpeed : 1000,
			    navigation: true,
			    pagination:false,
			    afterAction : syncPosition,
			    responsiveRefreshRate : 200,
			  });
			 
			  sync2.owlCarousel({
			    // items : 3,
			    // itemsDesktop      : [1199,10],
			    // itemsDesktopSmall     : [979,10],
			    // itemsTablet       : [768,8],
			    // itemsMobile       : [479,4],
			    pagination:false,
			    responsiveRefreshRate : 100,
			    afterInit : function(el){
			      el.find(".owl-item").eq(0).addClass("synced");
			    }
			  });
			 
			  function syncPosition(el){
			    var current = this.currentItem;
			    $("#sync2")
			      .find(".owl-item")
			      .removeClass("synced")
			      .eq(current)
			      .addClass("synced")
			    if($("#sync2").data("owlCarousel") !== undefined){
			      center(current)
			    }
			  }
			 
			  $("#sync2").on("click", ".owl-item", function(e){
			    e.preventDefault();
			    var number = $(this).data("owlItem");
			    sync1.trigger("owl.goTo",number);
			  });
			 
			  function center(number){
			    var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
			    var num = number;
			    var found = false;
			    for(var i in sync2visible){
			      if(num === sync2visible[i]){
			        var found = true;
			      }
			    }
			 
			    if(found===false){
			      if(num>sync2visible[sync2visible.length-1]){
			        sync2.trigger("owl.goTo", num - sync2visible.length+2)
			      }else{
			        if(num - 1 === -1){
			          num = 0;
			        }
			        sync2.trigger("owl.goTo", num);
			      }
			    } else if(num === sync2visible[sync2visible.length-1]){
			      sync2.trigger("owl.goTo", sync2visible[1])
			    } else if(num === sync2visible[0]){
			      sync2.trigger("owl.goTo", num-1)
			    }
			    
			  }
		}

	/*------------   OWL thumbs product END   ------------*/


	/*------------   STAR rating   ------------*/
		
		if(starRating.length){
	      $(starRating).each(function() {
	        var currentInitVal = parseFloat($(this).data('current-rating'));

	        $(this).barrating({
	              theme: 'css-stars',
	              showSelectedRating: false,
	              deselectable: true,
	              initialRating: currentInitVal
	          });
	      })
	  
	  	}
	  	if(stripeRating.length){
	  		$(stripeRating).barrating('show', {
	            theme: 'bars-1to10',
	            readonly: true
	        });
	  	}

	/*------------   STAR rating END   ------------*/


	/*------------   Input number   ------------*/
		
		


		$('.up').on('click', function(e){
			
			var inputVal = $(this).closest('.input_num_wrap').find('.input_num'),	
				currentVal = parseInt(inputVal.val());

			inputVal.val(currentVal += 1);

			e.preventDefault();
		});

		$('.down').on('click', function(e){
			var inputVal = $(this).closest('.input_num_wrap').find('.input_num'),
				currentVal = parseInt(inputVal.val());

			if(currentVal >= 1){
				inputVal.val(currentVal -= 1);
			}

			e.preventDefault();
		});

	/*------------   Input number END   ------------*/


	/*------------   Accordion   ------------*/
		
		if($('.accordion__item__link').length){
			$('.accordion__item__link').on('click', function(){
				$(this)
				.toggleClass('active')
				.next('.sub-menu')
				.slideToggle()
				.parents(".accordion__item")
				.siblings(".accordion__item")
				.find(".accordion__item__link")
				.removeClass("active")
				.next(".sub-menu")
				.slideUp();
			});  
		}
		$('.accordion__item__link').on('click', function(e) {
		    e.preventDefault();
		});

	/*------------   Accordion END   ------------*/


	/*------------   Swiper   ------------*/
		
		if (swiperBrandDouble.length){
			swiperBrandDouble.swiper({
				nextButton: '.swiper-button-next',
        		prevButton: '.swiper-button-prev',
		        slidesPerView: 5,
		        slidesPerColumn: 2,

		        breakpoints: {
				    // when window width is <= 320px
				    320: {
				      slidesPerView: 1
				    },
				    // when window width is <= 480px
				    768: {
				      slidesPerView: 2
				    },
				    // when window width is <= 640px
				    1200: {
				      slidesPerView: 3,
				      spiceBetween: 10
				    }
				  }
			});
		}

	/*------------   Swiper END   ------------*/


	/*------------   Mask   ------------*/
		
		if(mask.length){
			mask.mask("(999) 999 99 99");
		};

	/*------------   Mask END   ------------*/

	/*------------   OWL Brand   ------------*/

		if(owlBrandJs.length){
			owlBrandJs.owlCarousel({
		        items: 5,
		        navigation: true
			});
		}

	/*------------   OWL Brand END   ------------*/

	/*------------   Tabs   ------------*/

		if(tabs.length){
			tabs.responsiveTabs({
		        
			});
		}

	/*------------   Tabs END   ------------*/



	/*------------   POPUP   ------------*/

		if(popup.length){
			popup.on('click',function(){
			    var modal = $(this).data("popup");
			    $(modal).arcticmodal({
			    		beforeOpen: function(){
			    	}
			    });
			});
		};

	/*------------   POPUP  END  ------------*/

	 	
	
  	
  	// init default carousel

	if(owl.length){

		owl.each(function(){
			var current = $(this);

			current.owlCarousel({
				items : 1,
			    singleItem : true,
			    navigation: true
			});

			if (current.data('carousel-options') == 'custom-nav-btns'){
				current.on('click', ".carousel-btn", function(e){
					e.preventDefault();

					if ($(this).data('icon') == 'left') {
						current.data('owlCarousel').prev();
					}

					if ($(this).data('icon') == 'right') {
						current.data('owlCarousel').next();
					}
			
				})
			}
		})
			 
	}


	var innerFixedNav = $(".inner_page_nav.sidebar_nav");

	if (innerFixedNav.length){
		$(window).on('scroll', function(){
			if ($('body').scrollTop() > innerFixedNav.offset().top + innerFixedNav.height()) {
				$(".inner_page_nav.fixed").addClass('active');
			}
			else{
				$(".inner_page_nav.fixed").removeClass('active');
			}
		})


		$(".inner_page_nav").on('click', '.inner_page_nav__arrow', function(e){
			
			e.preventDefault();

			$('html, body').stop().animate({
	         scrollTop: 0
	      	}, 800);
		})
		
	}

	var innerFixedNav2 = $(".anchor__menu");

	if (innerFixedNav2.length){
		$(window).on('scroll', function(){
			if ($('body').scrollTop() > innerFixedNav2.offset().top + innerFixedNav2.height()) {
				$(".inner_page_nav.fixed").addClass('show');
			}
			else{
				$(".inner_page_nav.fixed").removeClass('show');
			}
		})


		$(".inner_page_nav").on('click', '.inner_page_nav__arrow', function(e){
			
			e.preventDefault();

			$('html, body').stop().animate({
	         scrollTop: 0
	      	}, 800);
		})
		
	}

	searchSection.on('keyup', '.search__field', function(event){
		$(event.delegateTarget).addClass('active');
		$('body').addClass('showOverlay');
	})

	searchSection.on('focus', '.search__field', function(event){
		$('body').addClass('showOverlay');
	})

	searchSection.on('blur', '.search__field', function(event){
		if (!$(this).val()){
			$(event.delegateTarget).removeClass('active');
			$('body').removeClass('showOverlay');
		}
	})

	searchSection.on('click', '.search__button--close', function(event){
		$(event.delegateTarget).removeClass('active').find('.search__field').val('');
		$('body').removeClass('showOverlay');
	})

	header.on('click', "#respNavToggleBtn", function(event) {
		$("body").toggleClass('showRespNav');

		$(document).on("click", function(event) {
	        if ($(event.target).closest(header).length) return;
	        
	        $("body").removeClass('showRespNav');
	        event.stopPropagation();
	    })

	})

	// if (select.length){
 // 		select.select2({
	// 		minimumResultsForSearch: -1
	// 	});
 // 	}

	if (styler.length){
 		styler.styler();
 	}

 	// toggle search logic

 	$("#searchToggleBtn").on('click', function(e) {
 		e.preventDefault();

 		$('body').toggleClass('showSearch');

 		$(document).on("click", function(event) {
	        if ($(event.target).closest("#searchToggleBtn, .main_search").length) return;
	        
	        $("body").removeClass('showSearch');
	        event.stopPropagation();
	    })
 	})

 	if (mainSlider.length){
 		mainSlider.owlCarousel({
			items : 1,
		    singleItem : true,
		    navigation: true,
		    navigationText : ['', ''],
		    slideSpeed : 1000,
		    addClassActive : true,
		    autoHeight : true,
		    mouseDrag :  false
		});
 	}


 	if(tooltip.length){
 		tooltip.tooltipster();
 	}

 	

	// skroll

	if(skroll.length){
		skroll.jScrollPane({
			verticalArrowPositions: 'after',
			horizontalArrowPositions: 'after'
		});
	};

	// init popup


 	$(".points_link").on('click', function(e) {
 		e.preventDefault();

 		$(this).parent().parent().parent().toggleClass('active');
 	})

 	

 // 	$(".points_link").on("click", function(e){
	// 	e.preventDefault();

	// 	$('body').find(".table__calendar--info").toggleClass("active");

 // 		$(document).on("click", function(event) {
	//         if ($(event.target).closest(".points_link").length) return;
	        
	//         $('body').find(".table__calendar--info").removeClass("active");
	//         event.stopPropagation();
	//     })
	// })


 	// inner navigation
	$('.inner_page_nav__list').on('click touchstart', 'a', function(event){
	    event.preventDefault();

	    var currentItem = $(this),
	        anchor = currentItem.attr('href');

	    
	    $(currentItem).closest('ul').find("li").removeClass("current");
	    $(currentItem).parent().addClass("current");
		
		if ($(anchor).length){
			$('html, body').stop().animate({
		        scrollTop: $(anchor).offset().top - 60
		    }, 1000, function(){
		        highlite_menu();
		    });
		}

	});

	highlite_menu();

	/*------------   tabs   ------------*/

	if(tabs.length){
		tabs.responsiveTabs();
	}


})


$(window).scroll(function(){
  highlite_menu();
})


var highlite_menu = function(){
  $('#content >*').each(function(){
        var highlight1 = function($el){
            var id = $el.attr('id'),
                $menulia = $('.inner_page_nav__list a');

            $('.inner_page_nav__list').find("li").removeClass('active');
            $('.inner_page_nav__list').find('a[href="#'+id+'"]').parent().addClass('active');
        }
        if ($(this)[0].id == 'contacts_page' && $(window).height() > 815){
            if ($(window).scrollTop() >= ( $(this).offset().top -  360 ) && $(window).scrollTop() < ( $(this).offset().top + 560 )    ){
                highlight1($(this));
                // console.log($(this))
            }

        } else {
            if ($(window).scrollTop() >= ( $(this).offset().top - 60) && $(window).scrollTop() < ( $(this).offset().top + 260 )    ){
                highlight1($(this));
            }
        }

    })
}
